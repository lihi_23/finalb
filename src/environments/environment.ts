// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyBj-gIpuQGFLvfAJwkOM3YTZNtpJjdaYsg",
    authDomain: "finalb-6374d.firebaseapp.com",
    databaseURL: "https://finalb-6374d.firebaseio.com",
    projectId: "finalb-6374d",
    storageBucket: "finalb-6374d.appspot.com",
    messagingSenderId: "743587346206",
    appId: "1:743587346206:web:083b2f84930d6375227b0f"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
