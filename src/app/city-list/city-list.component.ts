import { Router } from '@angular/router';
import { AuthService } from './../auth.service';
import { WeatherService } from './../weather.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-city-list',
  templateUrl: './city-list.component.html',
  styleUrls: ['./city-list.component.css']
})
export class CityListComponent implements OnInit {
  initalCities:any[] = [{cityName:'London'}, {cityName:'Paris'}, {cityName:'Tel Aviv'}, {cityName:'Jerusalem'}, {cityName:'Berlin'}, {cityName:'Rome'}, {cityName:'Dubai'}, {cityName:'Athens'}];
  cities:any[] = [{cityName:'London'}, {cityName:'Paris'}, {cityName:'Tel Aviv'}, {cityName:'Jerusalem'}, {cityName:'Berlin'}, {cityName:'Rome'}, {cityName:'Dubai'}, {cityName:'Athens'}];
  cities$;
  result$;
  dbData;
  userId:string;
  displayedColumns: string[] = ['cityName','data','temp','humidity','icon','predict','rainy'];
  
  getData(cityName:string):void{
   this.cities$ = this.weatherService.searchWeatherData(cityName);
   let cityObj = this.cities.find(city => city.cityName === cityName);
   this.cities$.subscribe(
     data => {
       cityObj.temperature = data.temperature;
       cityObj.image = data.image;
       cityObj.humidity = data.humidity;
     }
   )
  }

  
  public save(cityObject){
    this.weatherService.saveWeather(this.userId,cityObject);
  }

  predict(cityName:string,temperature:number,humidity:number){
    let cityObj = this.cities.find(city => city.cityName === cityName);
    this.result$ = this.weatherService.predictWeather(temperature,humidity);
    this.result$.subscribe(
      res => cityObj.predict = res
    )
  }

  deleteData(id:string,cityName:string){
    this.weatherService.deleteWeather(this.userId,id).subscribe(
      res =>{
        this.weatherService.getWeather(this.userId).subscribe(
          docs =>{
            const data:any[] = [];
            for(let document of docs){
              const row = document.payload.doc.data();
              row.id = document.payload.doc.id;
              data.push(row);
            }
            this.dbData = data;
            
            const dCities = this.initalCities.map(city =>{
              const cityExist = this.dbData.find(dbCity => dbCity.cityName === city.cityName);
              if(!!cityExist){
                return cityExist;
              }else{
                return city;
              }
            })

            this.cities = dCities;
            this.getData(cityName);
          }
        )
      }
    )
  }
  constructor(private weatherService:WeatherService,private auth:AuthService,private router:Router) { }

  ngOnInit(): void {
    this.auth.getUser().subscribe(
      res =>{
        this.userId = res.uid;
        this.weatherService.getWeather(this.userId).subscribe(
          docs =>{
            const data:any[] = [];
            for(let document of docs){
              const row = document.payload.doc.data();
              row.id = document.payload.doc.id;
              data.push(row);
            }
            this.dbData = data;
            const dCities = this.cities.map(city =>{
              const cityExist = this.dbData.find(dbCity => dbCity.cityName === city.cityName);
              if(!!cityExist){
                return cityExist;
              }else{
                return city;
              }
            })

            this.cities = dCities;
          }
        )
      } 

    )
  }

}
