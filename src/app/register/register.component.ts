import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';
import {FormControl, Validators} from '@angular/forms';

@Component({
  selector: 'register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  email:string;
  password:string;
  emailErrorMessage:string;
  isError:boolean = false; 

  onSubmit(){
      this.auth.register(this.email,this.password)
       .then(res => {
        this.router.navigate(['/login']);
      })
      .catch(err =>{
        this.isError = true;
        this.emailErrorMessage = err.message;
      })
    
  }

  clearError(){
    this.emailErrorMessage = '';
  }


  constructor(private auth:AuthService,private router:Router) { }

  ngOnInit(): void {
  }

}
