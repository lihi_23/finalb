export interface Weather {
    name:string,
    country:string,
    image:string,
    description:string,
    humidity:number
    temperature:number,
    lat?:number,
    lon?:number
}
     