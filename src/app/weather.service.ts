import { catchError, map } from 'rxjs/operators';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { Weather } from './interfaces/weather';
import { WeatherRaw } from './interfaces/weather-raw';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {
  private URL = "https://api.openweathermap.org/data/2.5/weather?q=";
  private KEY = "407ca8cfea6f7a4cc35238c152c8a5f4";
  private IMP = "units=metric";
  private predictURL = 'https://qfjz64i2y6.execute-api.us-east-1.amazonaws.com/beta';
  constructor( private http: HttpClient,private db:AngularFirestore) { }

  usersCollection:AngularFirestoreCollection = this.db.collection('users');
  weatherCollection:AngularFirestoreCollection;
  searchWeatherData(cityName:string):Observable<Weather>{
    return this.http.get<WeatherRaw>(`${this.URL}${cityName}&APPID=${this.KEY}&${this.IMP}`).pipe(
      map(data => this.transformWeatherData(data)),
      catchError(this.handleError)
    )

  }

  public getWeather(userId:string){
    this.weatherCollection = this.usersCollection.doc(userId).collection('weather');
    this.weatherCollection.get();
    return this.weatherCollection.snapshotChanges();
  }
  public saveWeather(userId,weather){
    this.weatherCollection = this.usersCollection.doc(userId).collection('weather');
    this.weatherCollection.add(weather);
   return this.weatherCollection.snapshotChanges();
  }
  
 public predictWeather(temprature:number,humidity:number){
    const body = {
      data:{
        temp:temprature,
        humidity:humidity
      }
    }
    return this.http.post<any>(this.predictURL,body).pipe(
      map(res =>{
        const result:string = res.body;
        return result;
      },catchError(this.handleError))
    )
  }

  deleteWeather(userId:string,weatherId:string){
    this.weatherCollection = this.usersCollection.doc(userId).collection('weather');
    this.weatherCollection.doc(weatherId).delete();
    return this.weatherCollection.snapshotChanges();
  }

  private handleError(res:HttpErrorResponse){
   console.log(res.error);
   return throwError(res.error || 'Server error');
  }

  private transformWeatherData(data:WeatherRaw):Weather{
    return{
      name:data.name,
      country:data.sys.country,
      humidity:data.main.humidity,
      image:`http://api.openweathermap.org/img/w/${data.weather[0].icon}.png`,
      description:data.weather[0].description,
      temperature:data.main.temp,
      lat:data.coord.lat,
      lon:data.coord.lon

    }
  }
    }

