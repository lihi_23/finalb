import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { AuthService } from '../auth.service';
import { Router, RouterModule } from '@angular/router';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent {

  userId;
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );

    
    logout(){
      this.authService.logout();
      this.router.navigate(['/login'])
    }
  constructor(private breakpointObserver: BreakpointObserver,public authService:AuthService,private router:Router) {}

  
  ngOnInit(): void {
    this.authService.getUser().subscribe(
      res => this.userId = res.uid
    )
  }


}
